import React from "react";
import "./TreinDrukte.css";
import { connect } from "react-redux";
import { changeSearchTerm } from "./actions";
import ScrollTrain from "./ScrollTrain";

// Voorbeeld waardes
const tijd = "10:04 --> 10:50";
const treindelen = 4;
const zitplaatsen = 405;
const klas_1e = 80;
const klas_2e = 292;
const klapstoelen = 33;

class TreinDrukte extends React.Component {


  render() {
    return (
      <div>
        <div className="infoContainer">
          <br></br>
          <div className="info">
            <div className="vanEnNaarContainer">Leiden Centraal</div>
            <div className="vanEnNaarContainer">Amsterdam Centraal</div>
            <h2>{tijd}</h2>
            <div className="infoRegel">
              <div className="links">Treindelen:</div>
              <div className="rechts">{treindelen}</div>
            </div>
            <div className="infoRegel">
              <div className="links">Zitplaatsen:</div>
              <div className="rechts">{zitplaatsen}</div>
            </div>
            <div className="infoRegel">
              <div className="links">1e klas:</div>
              <div className="rechts">{klas_1e}</div>
            </div>
            <div className="infoRegel">
              <div className="links">2e klas:</div>
              <div className="rechts">{klas_2e}</div>
            </div>
            <div className="infoRegel">
              <div className="links">Klapstoelen:</div>
              <div className="rechts">{klapstoelen}</div>
            </div>
          </div>
        </div>

        <ScrollTrain/>

      </div>
    );
  }
}

export default TreinDrukte;
