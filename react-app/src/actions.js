const CHANGE_SEARCHTERM = "CHANGE_SEARCHTERM";

export const changeSearchTerm = searchTerm => ({
  type: CHANGE_SEARCHTERM,
  payload: searchTerm,
});
