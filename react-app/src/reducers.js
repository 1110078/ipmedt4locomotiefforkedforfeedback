const CHANGE_SEARCHTERM = "CHANGE_SEARCHTERM";

export const searchTerm = (state = "", action) => {
  switch(action.type) {
    case CHANGE_SEARCHTERM:
      return action.payload;
    default:
      return state;
  }
};
